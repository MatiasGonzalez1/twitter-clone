import { CreatePostInputDTO, PostDTO } from '../dto'
import { PostRepository } from '../repository'
import { PostService } from '.'
import { validate } from 'class-validator'
import { ErrorException, ForbiddenException, NotFoundException } from '@utils'
import { CursorPagination } from '@types'
import { UserRepository } from '../../user/repository'
import { FollowerRepository } from '@domains/follower/repository'
export class PostServiceImpl implements PostService {
  constructor (private readonly repository: PostRepository, private readonly userRepository: UserRepository, private readonly followedRepository: FollowerRepository) {}

  async createPost (userId: string, data: CreatePostInputDTO): Promise<PostDTO> {
    await validate(data)
    return await this.repository.create(userId, data)
  }

  async deletePost (userId: string, postId: string): Promise<void> {
    const post = await this.repository.getById(postId)
    if (!post) throw new NotFoundException('post')
    if (post.authorId !== userId) throw new ForbiddenException()
    await this.repository.delete(postId)
  }

  async getPost (userId: any, postId: string): Promise<PostDTO> {
    const post = await this.repository.getById(postId)
    if (post) {
      const author = await this.userRepository.getById(post.authorId)
      const isFollowing = await this.followedRepository.getFollow(post.authorId, userId)

      if (author?.isPrivate === false || isFollowing) {
        return post
      } else if (author?.isPrivate && (isFollowing === true)) {
        return post
      } else {
        throw new ErrorException()
      }
    } else {
      throw new NotFoundException('Post do not exist')
    }
    // if (!post) {
    //   throw new NotFoundException('post')
    // } else if (post && isPrivate?.isPrivate == true) {
    //   throw new NotFoundException('Follow or die')
    // } else {
    //   return post
    // }
  }

  async getLatestPosts (userId: string, options: CursorPagination): Promise<PostDTO[]> {
    // TODO: filter post search to return posts from authors that the user follows
    return await this.repository.getAllByDatePaginated(options)
  }

  async getPostsByAuthor (userId: string, authorId: string, options: CursorPagination): Promise<PostDTO[]> {
    const author = await this.userRepository.getById(authorId)
    const allPost = await this.repository.getByAuthorId(authorId, options)
    const isFollowing = await this.followedRepository.getFollow(authorId, userId)

    if (author) {
      // the author isnt private or is the same person
      if (!author?.isPrivate || (author.id === userId)) {
        return allPost
      } else if (author?.isPrivate && isFollowing) {
        return allPost
      } else {
        throw new ErrorException()
      }
    } else {
      throw new NotFoundException('Author')
    }
  }
}
