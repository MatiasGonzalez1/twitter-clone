import { Request, Response, Router } from 'express'
import HttpStatus from 'http-status'
import 'express-async-errors'
import { FollowerServiceImpl } from '../service'
import { FollowerService } from '../service/follower.service'
import { FollowerRepositoryImpl } from '../repository'
import { db } from '@utils'

const service: FollowerService = new FollowerServiceImpl(new FollowerRepositoryImpl(db))

export const followerRouter = Router()

followerRouter.post('/follow/:id', async (req: Request, res: Response) => {
  const { id } = req.params
  const { userId } = res.locals.context

  const follower = await service.follow(userId, id)

  return res.status(HttpStatus.OK).json({ follower, message: 'You followed correctly' })
})

followerRouter.post('/unfollow/:id', async (req: Request, res: Response) => {
  const { id } = req.params
  const { userId } = res.locals.context

  await service.unfollow(userId, id)
  return res.status(HttpStatus.OK).json({ message: 'unfollow' })
})
