import { FollowerDTO } from '../dto'
import { FollowerRepository } from '../repository'
import { FollowerService } from './follower.service'

export class FollowerServiceImpl implements FollowerService {
  constructor (private readonly repository: FollowerRepository) {}

  async follow (userId: string, followedId: string): Promise<FollowerDTO> {
    // const validation = await this.repository.follow.
    const newFollow = await this.repository.follow(userId, followedId)
    return newFollow
  }

  async unfollow (userId: string, followedId: string): Promise<void> {
    await this.repository.unfollow(userId, followedId)
  }

  async getFollow (userId: string, followedId: string): Promise<FollowerDTO> {
    const follow = await this.repository.getFollow(userId, followedId)
    if (follow) {
      return follow
    } else {
      throw new Error('Not found')
    }
  }
}
