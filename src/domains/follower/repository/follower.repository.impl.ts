import { PrismaClient } from '@prisma/client'
import { FollowerRepository } from './follower.repository'
import { FollowerDTO } from '../dto'

export class FollowerRepositoryImpl implements FollowerRepository {
  constructor (private readonly db: PrismaClient) {}

  async follow (followedId: string, followerId: string): Promise<FollowerDTO> {
    return await this.db.follow.create({
      data: {
        followerId,
        followedId
      }
    })
  }

  async getFollow (followerId: string, followedId: string): Promise<FollowerDTO | null> {
    return await this.db.follow.findFirst({
      where: {
        followerId,
        followedId
      }
    })
  }

  async unfollow (followedId: string, followerId: string): Promise<void> {
    await this.db.follow.deleteMany({
      where: {
        followedId, followerId
      }
    })
  }
}
