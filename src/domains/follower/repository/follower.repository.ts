import { FollowerDTO } from '../dto'

export interface FollowerRepository {
  follow: (followerId: string, followedId: string) => Promise<FollowerDTO>
  unfollow: (followedId: string, followerId: string) => Promise<void>
  getFollow: (followerId: string, followedId: string) => Promise<FollowerDTO | null>
}
